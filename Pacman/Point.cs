﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManChallenge
{
    public class Point
    {
        public Point()
        {
            PointSet = new Dictionary<string, int>();
            PointSet.Add("Cherry", 100);
            PointSet.Add("Strawberry", 300);
            PointSet.Add("Orange", 500);
            PointSet.Add("Apple", 700);
            PointSet.Add("Melon", 1000);
            PointSet.Add("Galaxian", 2000);
            PointSet.Add("Bell", 3000);
            PointSet.Add("Key", 5000);
            PointSet.Add("Dot", 10);

            GhostsEaten = 0;
            LivesLeft = 3;
            TotalPoints = 5000;
            GainedLive = false;
        }

        public Dictionary<string, int> PointSet { get; set; }
        public int GhostsEaten { get; set; }
        public int LivesLeft { get; set; }
        public int TotalPoints { get; set; }
        public bool GainedLive { get; set; }

        public bool ProcessData(string data)
        {
            if (data == "VulnerableGhost")
            {
                switch (GhostsEaten)
                {
                    case 0:
                        TotalPoints = TotalPoints + 200;
                        GhostsEaten++;
                        break;
                    case 1:
                        TotalPoints = TotalPoints + 400;
                        GhostsEaten++;
                        break;
                    case 2:
                        TotalPoints = TotalPoints + 800;
                        GhostsEaten++;
                        break;
                    case 3:
                        TotalPoints = TotalPoints + 1600;
                        GhostsEaten++;
                        break;
                    default:
                        break;
                }
            }
            else if (data == "InvincibleGhost")
            {
                LivesLeft = LivesLeft - 1;
            }
            else
            {
                TotalPoints = TotalPoints + PointSet[data];
            }

            //-- PostProcessing

            if (TotalPoints >= 10000 && !GainedLive)
            {
                LivesLeft = LivesLeft + 1;
                GainedLive = true;
            }

            if (LivesLeft < 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}