﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            Point newGame = new Point();
            string gameString = "Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,InvincibleGhost,Melon,Galaxian,VulnerableGhost,VulnerableGhost,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Bell,Cherry,Strawberry,InvincibleGhost,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Dot,Orange,Apple,InvincibleGhost,VulnerableGhost,Key,InvincibleGhost,Dot,Dot,Dot,Dot,Dot,VulnerableGhost";
            string[] GameData = gameString.Split(',');
            int ParsingWord = 0;
            foreach (var word in GameData)
            {
                
                bool gameOver = newGame.ProcessData(word);
                if (gameOver)
                {
                    break;
                }
                ParsingWord++;
            }
            Console.WriteLine(newGame.TotalPoints + " " + newGame.LivesLeft);
            Console.ReadLine();

        }
    }
}